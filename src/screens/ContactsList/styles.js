import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  continer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#f0f0f0",
  },
  containerTopTab: {
    width: "100%",
    height: 60,
    justifyContent: "center",
    alignItems: "center",
  },
  mediumText: {
    fontSize: 18,
  },
  smallText: {
    fontSize: 12,
  },
  linearGradientBackground: {
    width: "100%",
    height: "100%",
    position: "absolute",
    zIndex: -1,
  },
  renderItem: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomColor: "gray",
    borderBottomWidth: 1,
    alignItems: "center",
  },
  flatlist: {
    width: "100%",
    // height: "90",
  },
  avatarContainer: {
    width: 40,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 20,
    backgroundColor: "white",
    marginRight: 10,
  },
  avatar: {
    width: 25,
    height: 25,
  },
  nameAvatarContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 20,
  },
  inputContainer: {
    backgroundColor: "white",
    height: 60,
    padding: 20,
  },
  checkbox: {
    marginRight: 30,
    zIndex: 1,
  },
});

export default styles;
