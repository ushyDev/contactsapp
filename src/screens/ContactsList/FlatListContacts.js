import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  FlatList,
  Image,
  CheckBox,
  TouchableOpacity,
} from "react-native";
import styles from "./styles";
import InputHeader from "./InputHeader";

const FlatListContacts = (props) => {
  const [QueryData, setQueryData] = useState([]);
  const [selectedList, setSelectedList] = useState([]);

  useEffect(() => {
    const sortArr = props.data.sort((a, b) =>
      a.last_name.localeCompare(b.last_name)
    );
    setQueryData(sortArr);
  }, [props.data]);

  const searchFilterFunction = (text) => {
    const newData = props.data.filter((item) => {
      const itemData = ` ${item.first_name.toUpperCase()} ${item.last_name.toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    const sortArr = newData.sort((a, b) =>
      a.last_name.localeCompare(b.last_name)
    );
    setQueryData(sortArr);
  };

  const addToSelectedList = (item) => {
    console.log("Contact selected", item.id);
    if (selectedList.includes(item.id)) {
      let newArr = selectedList.filter((e) => e !== item.id);
      setSelectedList(newArr);
    } else {
      setSelectedList([...selectedList, item.id]);
    }
  };

  const RendereItemContact = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => addToSelectedList(item)}
        style={styles.renderItem}
      >
        <View style={styles.nameAvatarContainer}>
          <View style={styles.avatarContainer}>
            {item.avatar === null ? (
              <Text>
                {item.first_name.slice(0, 1)}
                {item.last_name.slice(0, 1)}
              </Text>
            ) : (
              <Image style={styles.avatar} source={{ uri: item.avatar }} />
            )}
          </View>
          <View style={{ flexDirection: "column" }}>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.mediumText}>{item.first_name} </Text>
              <Text style={styles.mediumText}>{item.last_name}</Text>
            </View>
            <Text style={styles.smallText}>{item.email}</Text>
          </View>
        </View>
        <CheckBox
          style={styles.checkbox}
          value={selectedList.includes(item.id) ? true : false}
          onValueChange={() => addToSelectedList(item)}
        />
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      data={QueryData}
      renderItem={RendereItemContact}
      style={styles.flatlist}
      keyExtractor={(item) => item.id.toString()}
      ListHeaderComponent={
        <InputHeader onChangeText={(text) => searchFilterFunction(text)} />
      }
    />
  );
};

export default FlatListContacts;
