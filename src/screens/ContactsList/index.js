import React from "react";
import { View } from "react-native";

import styles from "./styles";
import TopTab from "./TopTab";
import FlatListContacts from "./FlatListContacts";

const ContactsList = (props) => {
  return (
    <View style={styles.continer}>
      <TopTab />
      <FlatListContacts data={props.data} />
    </View>
  );
};

export default ContactsList;
