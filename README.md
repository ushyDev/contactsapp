# ContactsApp

## Preview Web

Open this [link](https://contactsappuszacki.netlify.app/)

## Run project

- Install dependencies: `yarn install` (or `npm install`).

- Run on Web: `yarn web` (or `npm run web`).

- Run on iOS: `yarn ios` (or `npm run ios`).

- Run on Android: `yarn android` (or `npm run android`).

- Run on both Android & iOS: `yarn mobile` (or `npm run mobile`).


 
