import React from "react";
import { Text, View } from "react-native";
import styles from "./styles";
import { LinearGradient } from "expo-linear-gradient";

const TopTab = () => {
  return (
    <View style={styles.containerTopTab}>
      <Text style={[styles.mediumText, { color: "white" }]}>Contacts</Text>

      <LinearGradient
        colors={["#00FFFF", "green"]}
        style={styles.linearGradientBackground}
        start={[0, 1]}
        end={[1, 0]}
      />
    </View>
  );
};

export default TopTab;
