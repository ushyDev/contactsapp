import React from "react";
import { Text, View } from "react-native";
import styles from "./styles";

const Loading = () => {
  return (
    <View style={styles.continer}>
      <Text>Loading...</Text>
    </View>
  );
};

export default Loading;
