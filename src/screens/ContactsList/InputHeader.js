import React from "react";
import { View, TextInput } from "react-native";
import styles from "./styles";

const InputHeader = (props) => {
  return (
    <TextInput
      style={styles.inputContainer}
      onChangeText={props.onChangeText}
      value={props.value}
      placeholder="Search"
    />
  );
};

export default InputHeader;
