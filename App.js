import React, { useEffect, useState } from "react";

import ContactsList from "./src/screens/ContactsList";
import Loading from "./src/screens/Loading";

export default function App() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(
      "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
    )
      .then((response) => response.json())
      .then((json) => {
        setData(json);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => setLoading(false));
  }, []);

  return loading ? <Loading /> : <ContactsList data={data} />;
}
